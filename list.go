package main

import "fmt"

func main() {
	myList := make(map[string]string)

	for i := 0; i <= 9; i++ {
		myList[fmt.Sprintf("test%v", i)] = fmt.Sprintf("jason%v", i)
	}

	for _, k := range myList {
		fmt.Println(k)
	}
}
