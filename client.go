package main

import(
	"net"
	"fmt"
	"os"
	"encoding/json"
)

func main(){
	pUDPAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:7070")

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error ResolveUDPAddr: %s", err.Error())
		return
	}

	pUDPConn, err := net.DialUDP("udp", nil, pUDPAddr)

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error DialUDP: %s", err.Error())
		return
	}

	type TransData struct{
		MsgType int16 `json:"msg_type"`
		Msg string `json:"msg"`
	}

	myData := &TransData{
		MsgType: 201,
		Msg:     "connect me",
	}

	str, err := json.Marshal(myData)

	n, err := pUDPConn.Write([]byte(str))

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error WriteToUDP: %s", err.Error())
		return
	}

	_, _ = fmt.Fprintf(os.Stdout, "writed: %d", n)

	buf := make([]byte, 1024)
	n, _, err = pUDPConn.ReadFromUDP(buf)

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error ReadFromUDP: %s", err.Error())
		return
	}

	_, _ = fmt.Fprintf(os.Stdout, "readed: %d  %s", n, string(buf[:n]))
}
