package main

import (
	"fmt"
	//"io/ioutil"
	//"os"
	"log"
	"os/exec"
)

func main() {

	//占包
	fmt.Printf("------------------------  os包试玩  ----------------------------\n")

	//获取主机名 os.Hostname()  Hostname 函数会返回内核提供的主机名。
	//if name, err := os.Hostname(); err != nil{
	//	log.Fatalf("获取主机名失败: %s\n", err.Error())
	//}else{
	//	fmt.Printf("主机名： %s\n", name)
	//}

	//获取环境变量 os.Environ()  Environ 函数会返回所有的环境变量，返回值格式为“key=value”的字符串的切片拷贝。
	//Environ := os.Environ()
	//for k, v := range Environ{
	//	fmt.Printf("环境变量Key： %v, 环境变量Value: %s\n", k, v)
	//}

	//Getenv 函数会检索并返回名为 key 的环境变量的值。如果不存在该环境变量则会返回空字符串。
	//val := os.Getenv("Path")
	//fmt.Printf("Path的值是： %s\n", val)

	//Setenv 函数可以设置名为 key 的环境变量，如果出错会返回该错误
	//if err := os.Setenv("Jason", "123"); err != nil{
	//	log.Fatalf("出错了： %s\n", err.Error())
	//}else{
	//	fmt.Println("Success")
	//}

	//Exit 函数可以让当前程序以给出的状态码 code 退出。一般来说，状态码 0 表示成功，非 0 表示出错。程序会立刻终止，并且 defer 的函数不会被执行。
	//defer SayHello()
	//os.Exit(0)

	//Getuid 函数可以返回调用者的用户 ID。
	//uid := os.Getuid()
	//PrintMe(string(uid))

	//Getgid 函数可以返回调用者的组 ID。
	//PrintMe(string(os.Getgid()))

	//Mkdir 函数可以使用指定的权限和名称创建一个目录。如果出错，会返回 *PathError 底层类型的错误。
	//if err := os.Mkdir("Jason", 0777); err != nil{
	//	PrintMe(err.Error())
	//}
	//if err := ioutil.WriteFile("./Jason/app.log", []byte("我是Jason"),0777); err != nil{
	//	PrintMe("写入文件失败！")
	//}

	//Remove 函数会删除 name 指定的文件或目录。如果出错，会返回 *PathError 底层类型的错误。
	//RemoveAll 函数跟 Remove 用法一样，区别是会递归的删除所有子目录和文件。
	//if err := os.RemoveAll("Jason"); err != nil{
	//	PrintMe(err.Error())
	//}

	//os/exec 执行外部命令
	cmd := exec.Command("calc")
	_ = cmd.Run()

}

func SayHello() {
	log.Printf("Hello\n")
}

func PrintMe(msg string) {
	fmt.Printf("%v\n", msg)
}
