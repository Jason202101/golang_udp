package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

//func Hello(w http.ResponseWriter, req *http.Request) {
//	_, _ = w.Write([]byte("Hello"))
//}

func main() {
	//port := "8001"
	//http.HandleFunc("/hello", Hello)
	//fmt.Printf("Http server starting, port: %v\n", port)
	//if err := http.ListenAndServe(fmt.Sprintf(":%v", port), nil); err!=nil {
	//	fmt.Printf("Http server started failed, error message: %v", err.Error())
	//	return
	//}

	response, err := http.Get("https://www.bilibili.com/")

	if err != nil{
		log.Fatal(err)
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK{
		fmt.Printf("请求出错，错误状态码：%d", response.StatusCode)
	}

	result, err := ioutil.ReadAll(response.Body)

	if err != nil{
		fmt.Printf("读取内容出错：%v", err.Error())
	}

	fmt.Printf("%s", result)


}