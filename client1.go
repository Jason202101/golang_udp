package main

import (
	"fmt"
	"net"
	"encoding/json"
)

func main(){
	type SendData struct {
		MsgType int32 `json:"msg_type"`
		Msg string `json:"msg"`
	}

	data := &SendData{
		MsgType: 202,
		Msg: "connection",
	}

	str, err := json.Marshal(data)

	udpAddress, err := net.ResolveUDPAddr("udp", "127.0.0.1:7070")
	if err != nil{ panic(err) }
	udpConn, err := net.DialUDP("udp", nil, udpAddress)
	if err != nil{ panic(err) }

	buf := make([]byte, 2048)

	_, err = udpConn.Write(str)

	_, addr, err := udpConn.ReadFromUDP(buf)

	fmt.Printf("服务端的消息：%v", addr)
}
