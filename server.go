package main

import (
     "os"
     //"io/ioutil"
     "fmt"
     "net"
     "encoding/json"
     //"bufio"
)

func main(){
	//if file, err := os.Open("test.php"); err !=nil{
	//	panic(err)
	//}else{
	//	if fileBytes, err := ioutil.ReadAll(file); err == nil{
	//		fmt.Println(string(fileBytes))
	//	}
	//}

	//fileBytes, err := ioutil.ReadFile()

	//file, err := os.Create("hi.go")
	//CheckErr(err)

	//err2 := ioutil.WriteFile("123.txt", []byte("8723948723948"), 0777)
	//CheckErr(err2)

	//if addr, err := net.ResolveIPAddr("ip", "www.baidu.com"); err == nil{
	//	fmt.Println(addr)
	//	return
	//}else{
	//	panic(err)
	//}

	//domain := "www.baidu.com"
	//
	//// 百度，虽然只有一个域名，但实际上，他对应电信，网通，联通等又有多个IP地址
	//ns, err := net.LookupHost(domain)
	//if err != nil {
	//	fmt.Fprintf(os.Stderr, "Err: %s", err.Error())
	//	return
	//}
	//
	//for _, n := range ns {
	//	fmt.Fprintf(os.Stdout, "%s\n", n) // 115.239.210.26    115.239.210.27 这2个地址打开都是百度
	//}

	//port, err := net.LookupPort("tcp", "telnet") // 查看telnet服务器使用的端口
	//
	//if err != nil {
	//	fmt.Fprintf(os.Stderr, "未找到指定服务")
	//	return
	//}
	//
	//fmt.Fprintf(os.Stdout, "telnet port: %d", port)

	pUDPAddr, err := net.ResolveUDPAddr("udp", ":7070")

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error: %s", err.Error())
		return
	}

	pUDPConn, err := net.ListenUDP("udp", pUDPAddr)

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error: %s", err.Error())
		return
	}

	defer pUDPConn.Close()

	type TransData struct{
		MsgType int16 `json:"msg_type"`
		Msg string `json:"msg"`
	}

	for {

		buf := make([]byte, 2048)
		n , pUDPAddr, err := pUDPConn.ReadFromUDP(buf)

		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %s", err.Error())
			return
		}

		userData := &TransData{}

		 if err := json.Unmarshal(buf[:n], &userData); err != nil{
		 	panic(err)
		 }
		fmt.Fprintf(os.Stdout, "客户端发送的数据: 连接类型 -> %d , 消息 -> %s\n", userData.MsgType, userData.Msg)

		n, err = pUDPConn.WriteToUDP([]byte("好的！\n"), pUDPAddr)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %s", err.Error())
			return
		}
		fmt.Fprintf(os.Stdout, "writed: %d", n)
	}
}

func CheckErr(err error){
	if err != nil{
		fmt.Println(err.Error())
	}
}